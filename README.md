Messages WebApp
========

# Oldschool style
Visit `CrunchHelloWorld/messages` to see the overview of the messages. It is oldschool as there are manual page refreshes required for data update and a new window opens for adding a new messages

# Modern style
Visit `CrunchHelloWorld/angular` to open the app. The modern version uses the same spring app as backend for storing and retrieving messages. The backend is extended with a `RestController`class, so CRUD operations on messages are available as a REST Service.

The frontend is een angular.js app using ng-resource for easy use of the CRUD operations offered as a REST service. The two-way binding makes sure the list of messages is up-to-date.

Combined with bootstrap it makes a modern app.

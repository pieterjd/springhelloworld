package helloworld.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
//Tell Jackson where to get the id information, here from the field(=property) with the name id
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class,property="id")
public class Message {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@NotNull
	@Size(min=5)
	@Column
	private String sender;
	@NotNull
	@Size(min=10)
	@Column
	private String message;
	@Column
	@DateTimeFormat(pattern="hh:mm")
	private Date date = new Date();
	@Column (nullable = false, columnDefinition = "TINYINT(1)")
	private boolean isRead;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	//@JsonIgnore
	//user cannot be included in JSON, because it cause a JSON Serialization loop. User gets serialized, but this contains
	//messages, and messages contain users as well, and so and so on into infinity
	@JsonManagedReference
	private User user;
	
	public Message(){}
	

	public Message(String sender,String message){
		this(sender,message,new Date());
		
	}
	
	public Message(String sender, String message, Date date) {
		setSender(sender);;
		setMessage(message);
		setDate(date);
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getRead() {
		return isRead;
	}
	public void setRead(boolean read) {
		this.isRead = read;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	

}

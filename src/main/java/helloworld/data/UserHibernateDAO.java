package helloworld.data;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.springframework.stereotype.Repository;

import helloworld.model.Message;
import helloworld.model.User;

@Repository
public class UserHibernateDAO implements UserRepository {
	private SessionFactory factory;
	
	@Inject
	public UserHibernateDAO(SessionFactory factory){
		this.factory = factory;
	}
	
	private Session getSession(){
		return factory.openSession();
	}
	public User get(int id) {
		// TODO Auto-generated method stub
		return getSession().get(User.class, id);
	}

	public void save(User u) {
		Session s = getSession();
		Transaction t = null;
		try{
			t = s.beginTransaction();
			s.saveOrUpdate(u);
			t.commit();
		}
		catch(Exception e){
			if(t != null){
				t.rollback();
			}
			System.out.println("Error while saving user :(");
			e.printStackTrace();
			
		}
		finally {
			s.close();
		}

	}

	public void delete(User u) {
		getSession().delete(u);

	}

	public User findByUsername(String userName) {
		return (User) getSession().createQuery("from User where userName= :userName").setParameter("userName", userName).getResultList().get(0);
	}

	public List<User> getAll() {
		return (List<User>) getSession().createCriteria(User.class).list();
	}

}

package helloworld.data;

import java.util.List;

import helloworld.model.Message;

public interface MessageRepository {
	List<Message> getAll();
	public void save(Message m);
	public Message get(long id);
	public void delete(Message m);

}

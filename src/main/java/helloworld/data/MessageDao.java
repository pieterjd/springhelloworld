package helloworld.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import helloworld.model.Message;



public class MessageDao implements MessageRepository {
	private List<Message> messages;
	public MessageDao(){
		messages = new ArrayList<Message>();
		addDummyMessages();
	}
	
	private void addDummyMessages() {
		save(new Message("P-J","testing"));
		
		//Calculate yesterday
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -1);
		
		save(new Message("P-J","testing2",c.getTime()));
		
	}

	public List<Message> getAll() {
		return messages;
	}

	public void save(Message m) {
		m.setId(messages.size());
		messages.add(m);
		
	}

	public Message get(long id) {
		Message result = null;
		if(id<messages.size()){
			result = messages.get((int)id);
		}
		return result;
	}

	public void delete(Message m) {
		// TODO Auto-generated method stub
		
	}

	public void update(Message m) {
		// TODO Auto-generated method stub
		
	}

}

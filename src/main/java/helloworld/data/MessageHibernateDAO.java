package helloworld.data;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import helloworld.model.Message;

@Repository
@Transactional
public class MessageHibernateDAO implements MessageRepository {
	
	private SessionFactory sessionFactory;
	private Session session;
	
	@Inject
	public MessageHibernateDAO(SessionFactory sessionFactory){
		System.out.println("Hibernate DAO created");
		this.sessionFactory = sessionFactory;
		
	}
	
	private Session getSession(){
		return sessionFactory.openSession();
	}
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Message> getAll() {
		return (List<Message>)getSession().createCriteria(Message.class).addOrder(Order.desc("id")).list();

	}

	
	
	public void save(Message m) {
		Session s = getSession();
		Transaction t = null;
		try{
			t = s.beginTransaction();
			s.saveOrUpdate(m);
			t.commit();
		}
		catch(Exception e){
			if(t != null){
				t.rollback();
			}
			System.out.println("Error while saving :(");
			e.printStackTrace();
			
		}
		finally {
			s.close();
		}
	}

	public Message get(long id) {
		return getSession().get(Message.class, id);
	}

	

	public void delete(Message m) {
		getSession().delete(m);
		
	}

}

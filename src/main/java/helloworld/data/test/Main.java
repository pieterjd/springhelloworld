package helloworld.data.test;

import java.util.Date;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import helloworld.config.DataConfig;
import helloworld.data.MessageHibernateDAO;
import helloworld.data.MessageRepository;
import helloworld.data.UserHibernateDAO;
import helloworld.data.UserRepository;
import helloworld.model.Message;
import helloworld.model.User;


public class Main {

	public static void main(String[] args) {
		Configuration c =new Configuration()
				.addAnnotatedClass(Message.class)
				.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/pieterjd")
				.setProperty("hibernate.connection.username", "pieterjd")
				.setProperty("hibernate.connection.password", "19ABcd78")
				//next property creates tables if not yet existing
				.setProperty("hibernate.hbm2ddl.auto", "update")
				.setProperty("hibernate.show_sql", "true")
				;
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		
		ctx.register(DataConfig.class);
		ctx.refresh();
		SessionFactory factory = ctx.getBean(SessionFactory.class);
		MessageRepository repo = new MessageHibernateDAO(factory);
		UserRepository urepo = new UserHibernateDAO(factory);
		//Session s  =factory.openSession();
		
		
		
		Message m = repo.get(36);
		m.setMessage("updated on "+new Date());
		System.out.println("From message to user:"+m.getUser().getUserName());
		User u =new User();
		u.setUserName("pieterjd");u.setPassword("azertyuiop");urepo.save(u);
		m.setUser(u); u.getMessages().add(m);
		repo.save(m);urepo.save(u);
		
		//testing get
		User uFromDB = urepo.get(1);
		System.out.println(uFromDB.getId() +" "+uFromDB.getUserName());
		System.out.println(uFromDB.getMessages().size());
		System.out.println(urepo.findByUsername("pieterjd").getId());
		
	}

}

package helloworld.data;

import java.util.List;

import helloworld.model.User;

public interface UserRepository {
	public User get(int id);
	public void save(User u);
	public void delete(User u);
	public User findByUsername(String userName);
	public List<User> getAll();

}

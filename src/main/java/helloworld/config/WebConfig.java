package helloworld.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages="helloworld")

public class WebConfig extends WebMvcConfigurerAdapter{
	@Bean
	public ViewResolver getViewResolver(){
		InternalResourceViewResolver res = new InternalResourceViewResolver();
		res.setPrefix("/WEB-INF/views/");
		res.setSuffix(".jsp");
		res.setExposeContextBeansAsAttributes(true);
		return res;
	}
	
	
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(new MappingJackson2HttpMessageConverter());
	}


	@Override
	  public void configureDefaultServletHandling(
	        DefaultServletHandlerConfigurer configurer) {
	    configurer.enable();
	}

	//overriding this method, you tell spring where to find static resources
	//such as css, js, images, ...
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		// maps any reference to resources or subfolders in the jsps et all, to the folder resources in the webapp archive
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
	
	

}

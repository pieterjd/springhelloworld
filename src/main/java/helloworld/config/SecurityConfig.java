package helloworld.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.
                            configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.
                                    configuration.EnableWebMvcSecurity;
@Configuration
//MIND YOU: not just @EnableWebSecurity
@EnableWebMvcSecurity
 public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource dataSource;

		
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		//let's create our authentication system, here it is in memory
		/*
		auth.inMemoryAuthentication()
			.withUser("admin").password("admin").roles("ADMIN","USER").and()
			.withUser("pieterjd").password("pieterjd").roles("ADMIN");
		*/
		auth.jdbcAuthentication().dataSource(dataSource)
		.usersByUsernameQuery("select userName,password,true from User where userName=?")
		.authoritiesByUsernameQuery("select userName,'ROLE_USER' from User where userName=?")
		;
	}

	//let's create the authorization part here, some urls are public, others need authentication
	//only authentication for the add message part, others are public
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//super.configure(http);
		//by overriding this method, you need to add the formLogin explictly
		http
		
		
		.authorizeRequests()
		.antMatchers(HttpMethod.GET,"/addMessage").hasRole("ADMIN")
		
		.anyRequest().permitAll()
		.and()
		.formLogin().loginPage("/login")
		.and()
		.csrf().disable() // do not turn this off in reallife
		;
		
	}
	
}
package helloworld.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import helloworld.data.UserRepository;
import helloworld.model.Message;
import helloworld.model.User;

@RestController
@RequestMapping("/api/v1")
public class UserRestController {
	@Inject
	private UserRepository userRepository;
	
	@RequestMapping(value="/users",method=RequestMethod.GET)
	public List<User> getAll(){
		return userRepository.getAll();
	}
	
	@RequestMapping(value="users/{id}",method=RequestMethod.GET)
	public User getUser(@PathVariable("id") int id){
		System.out.println("Rest get");
		return userRepository.get(id);
	}
	
	
	
	
}

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ page session="false"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<title>Login Page</title>

<!-- 
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 -->
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">

</head>
<body onload='document.f.username.focus();'>
	<h3>Login with Username and Password with custom JSP</h3>
	<div class="container">
		<div class="form-group">
			<form name='f' action='login' method='POST'>
				<div>
					User: <input type='text' name='username' value='' class="form-control"/>
				</div>
				<div>
					Password: <input type='password' name='password' class="form-control"/>
				</div>
				<div>
					<button name="submit" type="submit" class="btn btn-default" >Login</button>
				</div>

				<!-- dees gevonden bij de source van default login -->
				<input name="_csrf" type="hidden"
					value="9075834c-a59e-41e0-b8b8-826aa5408059" />
				<!-- 
				<input name="_csrf" type="hidden"
					value="6829b1ae-0a14-4920-aac4-5abbd7eeb9ee" />
					 -->

			</form>
		</div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<!-- 
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	 -->
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false"%>
<%@ page isELIgnored="false" %>


	id: ${message.id}<br/>
	Sender: ${message.sender}<br/>
	Message: ${message.message}<br/>
	IsRead?: ${message.read}<br/>
	Date: ${message.date}<br/>


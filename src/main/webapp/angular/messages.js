//depends on ngResource
//depends on ngResource
var app = angular.module('demo', ['ngResource','ngRoute']);

app.config(function($routeProvider){
	$routeProvider
	.when("/",{
		templateUrl: "messages.html",
		controller: "MsgCtrl"
	})
	.when("user",{
		templateUrl: "users.html",
		controller: "UserCtrl"
	});
});
/*
app.factory('MsgService',function($resource){
	console.log('creating msgService');
	return $resource('http://localhost:8080/CrunchHelloWorld/api/v1/messages/:id',{id: "@id"});
});
*/
app.controller('MsgCtrl', function($scope, $http,$resource) {
	$scope.newMsg = {sender:"your name",message:"your message"};
	var MsgService = $resource('http://localhost:8080/CrunchHelloWorld/api/v1/messages/:id',{id: "@id"},{
	    //add my own new function. In this case, an update is a post to the general url without ids
	    update:{
	        url:'http://localhost:8080/CrunchHelloWorld/api/v1/messages',
	        method:'POST',
	        isArray:false
	    }
	});


	$scope.refresh = function(){
		MsgService.query(function(data){
			//query was succesfull
			$scope.messages = data;
		},function(){
			//query failed
			alert("Cannot load data. Something went wrong on the serverside.Please try again later.");
			
		});
	};
	
	$scope.refresh();
	$scope.viewMessage = function(msg){
		
		$scope.currentMessage = msg;
		msg.read = true;
		MsgService.update(msg);
	};

	$scope.addMsg = function(){
		var res = MsgService.save($scope.newMsg,function(data){
			//REST returns saved object if successfull, add it to the messages list
			console.log(data);
			$scope.messages.push(data);
		},function(response){
			//REST failure callback function
			console.log("creation failed.response below:");
			console.log(response);
		});
		
		
	}
	$scope.save = function(){
		console.log($scope.newMsg);
		$http.post('http://localhost:8080/CrunchHelloWorld/api/v1/messages',$scope.newMsg);
		window.location.href = 'index.html';
	};

	$scope.login = function(){
		var encoded = 'username='+encodeURIComponent("pieterjd")+
			"&password="+encodeURIComponent("azertyuiop");
		$http({
			method:'POST',
			url:'http://localhost:8080/CrunchHelloWorld/login',
			data: encoded,
			headers:{'Content-Type':'application/x-www-form-urlencoded'}
		})
		.success(function(data,status,headers,config){
			console.log('login success');
			console.log(data);
			console.log(status);
			console.log(headers);
			console.log(config);
			
		});
		
	}
});
app.controller('UserCtrl', function($scope, $http,$resource) {
	
	var UserService = $resource('http://localhost:8080/CrunchHelloWorld/api/v1/users/:id',{id: "@id"},{
	    //add my own new function. In this case, an update is a post to the general url without ids
	    update:{
	        url:'http://localhost:8080/CrunchHelloWorld/api/v1/users',
	        method:'POST',
	        isArray:false
	    }
	});


	$scope.refresh = function(){
		UserService.query(function(data){
			//query was succesfull
			$scope.users = data;
		},function(){
			//query failed
			alert("Cannot load data. Something went wrong on the serverside.Please try again later.");
			
		});
	};
	
	$scope.refresh();
	

	

	$scope.login = function(){
		var encoded = 'username='+encodeURIComponent("pieterjd")+
			"&password="+encodeURIComponent("azertyuiop");
		$http({
			method:'POST',
			url:'http://localhost:8080/CrunchHelloWorld/login',
			data: encoded,
			headers:{'Content-Type':'application/x-www-form-urlencoded'}
		})
		.success(function(data,status,headers,config){
			console.log('login success');
			console.log(data);
			console.log(status);
			console.log(headers);
			console.log(config);
			
		});
		
	}
});